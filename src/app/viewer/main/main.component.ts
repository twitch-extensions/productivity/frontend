import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {faPlay, faStop, faPause} from '@fortawesome/free-solid-svg-icons';
import {faTwitch} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {

  private http: HttpClient;
  public time = 0;
  public finalTime = 0;
  public pausedTime = 0;
  public paused = false;
  public interval;
  public userName;
  public photo;
  private parts;
  private payload;
  private host;
  private twitchID;
  public faPlayIcon;
  public faStopIcon;
  public faPauseIcon;
  public faTwitch;
  private streamID;
  public isLive: boolean;

  constructor(http: HttpClient) {
    this.http = http;
    this.faPlayIcon = faPlay;
    this.faStopIcon = faStop;
    this.faPauseIcon = faPause;
    this.faTwitch = faTwitch;
    this.isLive = false;
    this.host = 'http://localhost:9600';
  }

  ngAfterViewInit(): void {
    this.getUser();
    this.getStream();
  }

  ngOnInit(): void {
  }

  private getStream(): number{
    this.http.get(`${this.host}/stream/`)
      .subscribe(
        success => {
          // @ts-ignore
          this.streamID = success.id;
          // @ts-ignore
          if (success.id !== undefined){
            this.isLive = true;
          }
        }, error => {
          // @ts-ignore
          // window.Twitch.ext.rig.log(error);
          console.error(error);
        }
      );
    return 1;
  }

  private getUser(): void {
    console.log('Get user');
    let user = {};
    // @ts-ignore
    window.Twitch.ext.onAuthorized(
      (auth) => {
        this.parts = auth.token.split('.');
        this.payload = JSON.parse(window.atob(this.parts[1]));
        if (this.payload.user_id) {
          // @ts-ignore
          this.twitchID = this.payload.user_id;
          this.http.get(`${this.host}/user/${this.payload.user_id}`)
            .subscribe(
              success => {
                // @ts-ignore
                this.userName = success.display_name;
                // @ts-ignore
                this.photo = success.profile_image_url;
                user = success;
              }, error => {
                // @ts-ignore
                // window.Twitch.ext.rig.log(error);
                console.error(error);
              }
            );
        }
      }
    );
    // @ts-ignore
    this.userName = user.display_name;
    // @ts-ignore
    this.photo = user.profile_image_url;
  }

  public startTime(): void {
    alert('time starts');
    this.interval = setInterval(e => {
      if (!this.paused) {
        this.time = this.time + 1;
      }
    }, 1000);
  }

  public pauseTime(): void {
    this.paused = true;
  }

  public resumeTime(): void {
    this.paused = false;
  }

  public endTime(): void {
    if (this.isLive === true){
      clearInterval(this.interval);
      this.finalTime = this.time;
      this.time = 0;
      this.http.patch(`${this.host}/save-time`, {
        streamID: this.streamID,
        twitchId: this.twitchID,
        name: this.userName,
        photo: this.photo,
        time: this.finalTime
      }).subscribe(
        result => {
          console.log('success', result);
        }, error => {
          console.error('Error', error);
        }
      );
    }
  }

}
